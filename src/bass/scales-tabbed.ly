\version "2.20.0"
tuning = #bass-five-string-tuning

\header {
  title = "Scales (Tabbed for 5)"
  author = "Quinn Bohner"
}
\score {
  \header { piece = "C Maj." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    c,,2 d e f g a b c c d e f g a b c
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    c,,2 d e f g a b c c d e f g a b c
    }
  }
>>
}
\score {
  \header { piece = "B Maj." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    b,,,2 cis dis e fis gis ais b b cis dis e fis gis ais b
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    b,,,2 cis dis e fis gis ais b b cis dis e fis gis ais b
    }
  }
>>
}
\score {
  \header { piece = "E Maj." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    e,,2 fis gis a b cis dis e e fis gis a b cis dis e
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    e,,2 fis gis a b cis dis e e fis gis a b cis dis e
    }
  }
>>
}
\score {
  \header { piece = "A Maj." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    a,,2 b cis d e fis gis a a b cis d e fis gis a
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    a,,2 b cis d e fis gis a a b cis d e fis gis a
    }
  }
>>
}
\score {
  \header { piece = "D Maj." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    d,,2 e fis g a b cis d d e fis g a b cis d
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    d,,2 e fis g a b cis d d e fis g a b cis d
    }
  }
>>
}
\score {
  \header { piece = "G Maj." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    g,,2 a b c d e fis g g a b c d e fis g
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    g,,2 a b c d e fis g g a b c d e fis g
    }
  }
>>
}

\score {
  \header { piece = "C Min." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    c,,2 d ees f g aes bes c c d ees f g aes bes c
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    c,,2 d ees f g aes bes c c d ees f g aes bes c
    }
  }
>>
}
\score {
  \header { piece = "B Min." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    b,,,2 cis d e fis g a b b cis d e fis g a b
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    b,,,2 cis d e fis g a b b cis d e fis g a b
    }
  }
>>
}
\score {
  \header { piece = "E Min." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    e,,2 fis g a b c d e e fis g a b c d e
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    e,,2 fis g a b c d e e fis g a b c d e
    }
  }
>>
}
\score {
  \header { piece = "A Min." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    a,,2 b c d e f g a a b c d e f g a
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    a,,2 b c d e f g a a b c d e f g a
    }
  }
>>
}
\score {
  \header { piece = "D Min." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    d,,2 e f g a bes c d d e f g a bes c d
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    d,,2 e f g a bes c d d e f g a bes c d
    }
  }
>>
}
\score {
  \header { piece = "G Min." }
<<
\new Voice {
  \clef "bass_8"
  \key c \major
  \relative {
    g,,2 a bes c d ees f g g a bes c d ees f g
  }
}
\new TabStaff \with {
    stringTunings = \tuning
  } {
    \relative {
    g,,2 a bes c d ees f g g a bes c d ees f g
    }
  }
>>
}
