{

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09";
  };
  description = "exercises for getting good.";

  outputs = { self, nixpkgs, ... } @ inputs: 
  let
    supportedSystems = [ "x86_64-linux" ];
    forAllSystems = f: nixpkgs.lib.genAttrs supportedSystems (system: f system);
  in {

    overlay = final: prev: {

      bass = with final; stdenv.mkDerivation {
        name = "bass-exercises";
        FONTCONFIG_FILE = pkgs.makeFontsConf { fontDirectories = [ ]; };
        nativeBuildInputs = [ lilypond ];
        phases = [ "unpackPhase" "buildPhase" "installPhase" ];
        src = ./src/bass;
        buildPhase = ''
          lilypond *.ly
        '';
        installPhase = ''
          install -Dm644 *.pdf -t $out
        '';
      };

    };

    packages = forAllSystems (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ self.overlay ];
      };
      in {
        bass = pkgs.bass;
      });

  };

}
